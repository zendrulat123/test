package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"

	"gitlab.com/zendrulat123/test/cmd"
	m "gitlab.com/zendrulat123/test/monitor"
	ttt "gitlab.com/zendrulat123/test/oscom"
)

var (
	input = flag.String("input", "", "pwd")
)

func main() {
	cmd.Execute()
	flag.Parse()

	args := []string{"input"}
	cmd := exec.Command(args[0], args[1:]...)
	b, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("running failed %v", err)
	}
	fmt.Printf("%s\n", b)
	fmt.Println("c - create, d - delete, r - read, w - write, rp - replace, cp - copy \n")
	fmt.Println("info - information, z - zip, zo - unzip, sc - scan \n")
	fmt.Println("commands - pwd, tree, ptree \n")
	fmt.Println("---------------------")
	R := bufio.NewReader(os.Stdin)
	go m.NewMonitor(100)
	RR := ""
	for {
		t := ttt.Getcom(R)
		switch t {
		case "c":
			fmt.Print("create what? ")
			tt := ttt.Getcom(R)
			ttt.Createfile(tt)
		case "d":
			fmt.Print("delete what? ")
			tt := ttt.Getcom(R)
			ttt.Deletefile(tt)
		case "r":
			fmt.Print("read what? ")
			tt := ttt.Getcom(R)
			RR = ttt.ReadFile(tt)
			fmt.Println(RR)
		case "w":
			fmt.Print("write what? ")
			tt := ttt.Getcom(R)
			ttt.WriteFile(tt)
		case "rp":
			fmt.Print("replace what? First location? ")
			tt := ttt.Getcom(R)
			fmt.Print("Second location? ")
			tt2 := ttt.Getcom(R)
			ttt.Replace(tt, tt2)
		case "cp":
			fmt.Print("copy what? First location? ")
			tt := ttt.Getcom(R)
			fmt.Print("Second location? ")
			tt2 := ttt.Getcom(R)
			ttt.Copy(tt, tt2)
		case "info":
			fmt.Print("info on what? ")
			tt := ttt.Getcom(R)
			ttt.GetInfo(tt)
		case "z":
			fmt.Print("file to add to zip? ")
			tt := ttt.Getcom(R)
			fmt.Print("first zip file? ")
			tt2 := ttt.Getcom(R)
			fmt.Print("second zip file? ")
			tt3 := ttt.Getcom(R)
			fmt.Print("third zip file? ")
			tt4 := ttt.Getcom(R)
			ttt.ZipUp(tt, tt2, tt3, tt4)
		case "zo":
			fmt.Print("What file to unzip? ")
			tt := ttt.Getcom(R)
			fmt.Print("where would you like to unzip it? ")
			tt2 := ttt.Getcom(R)
			ttt.ZipOpen(tt, tt2)
		case "sc":
			fmt.Print("what file to scan? ")
			tt := ttt.Getcom(R)
			fmt.Print("what word are you replacing? ")
			tt2 := ttt.Getcom(R)
			fmt.Print("With what words? ")
			tt3 := ttt.Getcom(R)
			ttt.ScanWords(tt, tt2, tt3)
		case "pwd":
			ttt.PWD()
		case "tree":
			ttt.Tree()
		}
		//https://scene-si.org/2018/08/06/basic-monitoring-of-go-apps-with-the-runtime-package/

	}

}
